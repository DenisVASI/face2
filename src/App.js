import React from 'react';
import './sass/App.sass';
import './sass/fonts/fonts.sass';
import Camera from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import axios from 'axios';

const dataURLtoFile = (dataurl, filename) => {
    const arr = dataurl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n) {
        u8arr[n-1] = bstr.charCodeAt(n-1);
        // u8arr[n] = bstr.charCodeAt(n);
        n -= 1 // to make eslint happy
    }
    return new File([u8arr], filename, { type: mime })
};

class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            status: null
        }
    }

    onTakePhoto = (dataUri) => {
        setTimeout(()=>{
            const file = dataURLtoFile(dataUri);
            const data = new FormData();
            data.append('img', file, file.name);
            const config = {
                headers: { 'Content-Type': 'multipart/form-data' }
            };
            axios.post('api/upload', data, config).then(response => {
                this.setState({status: true}, ()=>{
                    setTimeout(()=>{this.setState({status: null})}, 2000);
                })
            }).catch(()=>{
                this.setState({status: false}, ()=>{
                    setTimeout(()=>{this.setState({status: null})}, 2000);
                })
            });
        }, 1000);
    };

    onCameraError = (error) =>{
        console.error('onCameraError', error);
    };

    render(){
        const {status} = this.state;
        return (
            <div className="App">
                <div className={'content'}>
                    <div id="logo">
                        FaceAuth 2
                    </div>
                    <div className="cam-module">
                        <div className={status == null ? '' : !status ? 'fail' : 'ok'}>{status == null ? '' : !status ? 'fail' : 'success'}</div>
                        <Camera
                            onCameraError = { (error) => { this.onCameraError(error); } }
                            idealResolution = {{width: 500, height: 500}}
                            imageCompression = {0.97}
                            isMaxResolution = {false}
                            isImageMirror = {false}
                            isDisplayStartCameraError = {true}
                            sizeFactor = {1}
                            onTakePhoto={(dataUri) => {
                                this.onTakePhoto(dataUri);
                            }}
                        />
                    </div>

                </div>
            </div>
        );
    }

}

export default App;
